import Vue from "vue";
import App from "./App.vue";
import VueRouter from "vue-router";
import InicioSesion from "@/components/InicioSesion.vue";
import EventoCrear from "@/components/EventoCrear.vue";

Vue.config.productionTip = false;

Vue.use(VueRouter);

const routes = [
  {
    path: "/InicioSesion",
    component: InicioSesion
  },
  {
    path: "/EventoCrear",
    component: EventoCrear
  },
  {
    path: "/",
    redirect: "/InicioSesion"
  }
];

const router = new VueRouter({
  routes
});

new Vue({
  render: h => h(App),
  router
}).$mount("#app");
